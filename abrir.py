import requests


class ViaCEP:
    def __init__(self, cep):
        self.cep = cep

    def getDadosCEP(self):
        url_api = f"http://www.viacep.com.br/ws/{self.cep}/json"
        req = requests.get(url_api)
        return req.json()


cep = ViaCEP("04067030")
cep.getDadosCEP()
#
