class Programa(object):
    def __init__(self, nome: str, tipo: str, ano: int):
        self._nome = nome
        self._tipo = tipo
        self._ano = ano

    @property
    def nome(self):
        nome = self._nome.title()
        return nome

    @property
    def ano(self):
        return self._ano

    @ano.setter
    def ano(self, valor):
        self._ano = valor

    def __str__(self):
        return f" nome: {self._nome}, tipo: {self._tipo}, ano: {self._ano}"


class Filme(Programa):
    def __init__(self, nome: str, tipo: str, ano: int, legendado: str):
        super().__init__(nome, tipo, ano)
        self._legendado = legendado

    def __str__(self):
        return f"nome: {self._nome}, tipo: {self._tipo}, ano: {self._ano}, temporada: {self._legendado}"


class Serie(Programa):
    def __init__(self, nome: str, tipo: str, ano: int, temporada: str):
        super().__init__(nome, tipo, ano)
        self._temporada = temporada

    def __str__(self):
        return f" nome: {self._nome}, tipo: {self._tipo}, ano: {self._ano}, temporada: {self._legendado}"


class Playlist(object):
    def __init__(self, nome, lista):
        self.nome = nome
        self.lista = lista

    def __getitem__(self, item):
        return self.lista[item]

    def __len__(self):
        return len(self.lista)

    def __str__(self):
        return self.lista


z = Programa("teste", "outro", 1998)
print(z)

vingadores = Filme("Primeiro", "Ficção", 1998, "legendado")
star_wars = Filme("Star Wars", "Ficção", 1984, "Legendado")
serie = Serie("Friends", "Humor", 1998, "12")

x = vingadores.ano
print(x)

serie.ano = 2010
y = serie.ano
print(y)

play = [vingadores, star_wars, serie]

spotfy = Playlist("Minha musica", play)

for musica in spotfy:
    print(musica)

if __name__ == "__main__":
    pass

#
