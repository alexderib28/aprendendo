class Programa:
    def __init__(self, nome, tipo, ano):
        self._nome = nome
        self._ano = ano
        self._tipo = tipo
        self._like = 0

    def curtir(self):
        self._like += 1
        return self._like

    @property
    def nome(self):
        nome = self._nome.title()
        return nome

    @property
    def ano(self):
        return self._ano

    @ano.setter
    def ano(self, ano):
        self._ano = ano

    def __str__(self):
        return f"{self._nome}, {self._ano}, {self._like}"


class Filme(Programa):
    def __init__(self, nome, tipo, ano, duration):
        super().__init__(nome, tipo, ano)
        self.duration = duration

    def __str__(self):
        return f" {self._nome}, {self._ano}, {self._like}, {self.duration}"


class Serie(Programa):
    def __init__(self, nome, tipo, ano, temporada):
        super().__init__(nome, tipo, ano)
        self.temporada = temporada

    def __str__(self):
        return f" {self._nome}, {self._ano}, {self._like}, {self.temporada}"


class Playlist:
    def __init__(self, nome, programa):
        self.nome = nome
        self._programa = programa

    def __getitem__(self, item):
        return self._programa[item]

    @property
    def listagem(self):
        return self._programa

    def __len__(self):
        return len(self._programa)


vingadores = Filme("Vingadores", "1", "2008", "120")
atlanta = Serie("Atlanta", "2", "1999", "varias")
maisuma = Filme("história", "1", "2020", "120")

vingadores.curtir()
vingadores.curtir()
vingadores.curtir()
vingadores.curtir()

atlanta.curtir()
atlanta.curtir()
atlanta.curtir()

maisuma.curtir()
maisuma.curtir()

lista = [vingadores, atlanta]

play = Playlist("fim de semana", lista)

for programa in play:
    print(programa)

print(f"\nTamanho da playlist: {len(play)}")

#
