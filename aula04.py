class Graph:
    def __init__(self, n):
        self.adj = {}

    def __repr__(self):
        return f"{self.adj}"

    def add_arrow(self, a, b, peso=1):
        self.adj[a, b] = peso

    def add_edge(self, a, b, peso=1):
        self.add_arrow(a, b, peso)
        self.add_arrow(b, a, peso)

    def get_neighboards(self, a):
        neighbors = []
        for i, w in enumerate(self.adj[a]):
            if w > 0:
                neighbors.append(i + 1)
        return neighbors

    def __len__(self):
        return self.adj.shape[0]


def dfs(g, v):
    visited = [0] * len(g)
    visited[v] = 1
    for w in g.get_neighboards(v):
        if visited[w] > 0:
            continue
        dfs(g, w)


g = Graph(4)
g.add_edge(1, 0)
g.add_edge(1, 2)
g
len(g)

#
